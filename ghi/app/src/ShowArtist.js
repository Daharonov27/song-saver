import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom';


function Artist() {
    const [artist, setArtist] = useState({});
    const [songs, setSongs] = useState([]);

    let params = useParams();

    async function fetchArtist() {
        const getArtistUrl = `http://localhost:8080/api/artists/${params.artistId}`
        const response = await fetch(getArtistUrl);
        const data = await response.json();
        setArtist(data);
    }

    async function fetchSongs() {
        const getSongsByArtistUrl = `http://localhost:8080/api/artists/${params.artistId}/songs/`;
        const response = await fetch(getSongsByArtistUrl);
        const data = await response.json();
        setSongs(data.songs);
    }

    useEffect(() => {
        fetchArtist();
        fetchSongs();
    }, []);

    return (
      <div>
        <h1>{artist.name}</h1>
        <h3>Genre: {artist.genre}</h3>
        <table className="table">
            <thead>
                <tr>
                    <th>
                        Song Name
                    </th>
                    <th>
                        Play Song
                    </th>
                </tr>
            </thead>
            <tbody>
                {songs.map(song => {
                    return (
                        <tr key={song.id}>
                            <td>{song.name}</td>
                            <td></td>
                        </tr>
                    )
                })}
            </tbody>
        </table>

      </div>
    );

  }

  export default Artist;
