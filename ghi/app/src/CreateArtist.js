import React, { useState } from 'react'

function ArtistForm() {
    const [name, setName] = useState('');
    const [genre, setGenre] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleGenreChange = (event) => {
        const value = event.target.value;
        setGenre(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = name;
        data.genre = genre;

        const artistsUrl = 'http://localhost:8080/api/artists/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(artistsUrl, fetchConfig);
        if (response.ok) {
            const newArtist = await response.json();
            setName('');
            setGenre('');
        }
    }
    return (
        <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Add a new Artist</h1>
                <form onSubmit={handleSubmit} id="create-artist-form">
                  <div className="form-floating mb-3">
                    <input onChange={handleNameChange} value={name} placeholder="Artist's Name" required type="text" name="name" id="name" className="form-control"/>
                    <label htmlFor="name">Artist's Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={handleGenreChange} value={genre} placeholder="Genre" required type="text" name="genre" id="genre" className="form-control"/>
                    <label htmlFor="genre">Genre</label>
                  </div>
                  <button className="btn btn-primary">Add Artist</button>
                </form>
              </div>
            </div>
          </div>
      );
}

export default ArtistForm;
