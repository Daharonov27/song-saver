import React, { useEffect, useState } from 'react'

function SongList() {
    const [songs, SetSongs] = useState([]);

    async function loadSongs() {
        const response = await fetch('http://localhost:8080/api/songs/');
        if (response.ok) {
            const data = await response.json();
            const songInformationArrPromises = data.map(async song => {
                const url = `http://localhost:8080/api/songs/${song.id}`;
                const getASongResponse = await fetch(url);
                const responseBody = await getASongResponse.json();
                return responseBody
            })

            const SongInformationArray = await Promise.all(songInformationArrPromises);
            SetSongs(SongInformationArray);
        } else {
            console.error(response)
        }
    }

    useEffect(() => {
        loadSongs();
    }, []);

    return (
        <>
          <h3 style={{marginTop: "30px"}}>All Your Songs!</h3>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>
                  Name
                </th>
                <th>
                  Artist
                </th>
                <th>
                  Genre
                </th>
              </tr>
            </thead>
            <tbody>
              {songs.map(song => {
                return (
                  <tr key={song.id}>
                    <td>{song.name}</td>
                    <td>{song.artist.name}</td>
                    <td>{song.artist.genre}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </>
      );
}

export default SongList;
