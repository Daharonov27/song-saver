import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom';

function ArtistList() {
    const [artists, setArtists] = useState([]);
    const [artist, setArtist] = useState('');

    async function loadArtists() {
        const response = await fetch('http://localhost:8080/api/artists/');
        if (response.ok) {
            const data = await response.json();
            const artistInfoArrayPromises = data.map(async artist => {
                const url = `http://localhost:8080/api/artists/${artist.id}`;
                const getAnArtistResponse = await fetch(url);
                const responseBody = await getAnArtistResponse.json();
                return responseBody
            })

            const artistInformationArray = await Promise.all(artistInfoArrayPromises);
            setArtists(artistInformationArray);
        } else {
            console.error(response)
        }
    }


    useEffect(() => {
        loadArtists();
    }, []);

    return (
        <>
          <h3 style={{marginTop: "30px"}}>Saved Artists</h3>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>
                  Name
                </th>
                <th>
                  Genre
                </th>
                {/* <th>
                  Songs {figure out how to add songs in for each artist}
                </th> */}
              </tr>
            </thead>
            <tbody>
              {artists.map(artist => {
                console.log(artist.id)
                return (
                  <tr key={artist.id}>
                    <td>
                        <div>
                            <Link to={`/artists/${artist.id}`}>{artist.name}</Link>
                        </div>
                    </td>
                    <td>{artist.genre}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </>
      );
}

export default ArtistList;
