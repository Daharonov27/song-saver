import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SongList from './SongsList';
import ArtistList from './ArtistList';
import ArtistForm from './CreateArtist';
import Artist from './ShowArtist';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="songs" element={<SongList />} />
          <Route path="artists" element={<ArtistList />} />
          <Route path="artists/new" element={<ArtistForm />} />
          <Route path="artists/:artistId" element={<Artist />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
