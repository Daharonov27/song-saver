from django.db import models


class Artist(models.Model):
    name = models.CharField(max_length=100)
    genre = models.CharField(max_length=100)


class Song(models.Model):
    name = models.CharField(max_length=100)
    # we have an id attribute but it'll be added on by default
    artist = models.ForeignKey(
        Artist,
        related_name="songs",
        on_delete=models.CASCADE,
    )
