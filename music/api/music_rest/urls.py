from django.urls import path
from .views import list_artists, show_artist, list_songs, show_song, get_songs_by_artist


urlpatterns = [
    path("artists/", list_artists, name="list_artists"),
    path("artists/<int:id>/", show_artist, name="show_artist"),
    path("songs/", list_songs, name="list_songs"),
    path("songs/<int:id>/", show_song, name="show_song"),
    path("artists/<int:artistId>/songs/", get_songs_by_artist, name="get_song_by_artist"),
]
