from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse
from music_rest.encoders import ArtistListEncoder, ArtistDetailEncoder, SongDetailEncoder, SongListEncoder
from .models import Artist, Song


@require_http_methods(["GET", "POST"])
def list_artists(request):
    if request.method == "GET":
        artists = Artist.objects.all()
        return JsonResponse(
            artists,
            encoder=ArtistListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        artist = Artist.objects.create(name=content["name"], genre=content["genre"])
        # the above is long hand syntax but could also be written as:
        # artist = Artist.objects.create(**content)
        return JsonResponse(
            artist,
            encoder=ArtistDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET"])
def show_artist(request, id):
    if request.method == "GET":
        artist = Artist.objects.get(id=id)
        return JsonResponse(
            artist,
            encoder=ArtistDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def list_songs(request):
    if request.method == "GET":
        songs = Song.objects.all()
        return JsonResponse(
            songs,
            encoder=SongListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            artist = Artist.objects.get(id=content["artist_id"])
            song = Song.objects.create(name=content["name"], artist=artist)
            return JsonResponse(
                song,
                encoder=SongDetailEncoder,
                safe=False,
            )
        except Artist.DoesNotExist:
            return JsonResponse(
                {"message": "Artist does not exist."}
            )


@require_http_methods(["GET"])
def show_song(request, id):
    if request.method == "GET":
        song = Song.objects.get(id=id)
        return JsonResponse(
            song,
            encoder=SongDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET"])
def get_songs_by_artist(request, artistId):
    if request.method == "GET":
        songs = Song.objects.filter(artist_id=artistId)
        return JsonResponse(
            { 'songs': songs },
            encoder=SongListEncoder,
        )
