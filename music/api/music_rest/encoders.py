from common.json import ModelEncoder
from .models import Artist, Song


class ArtistListEncoder(ModelEncoder):
    model = Artist
    properties = [
        "id",
    ]


class ArtistDetailEncoder(ModelEncoder):
    model = Artist
    properties = [
        "id",
        "name",
        "genre",
    ]


class SongListEncoder(ModelEncoder):
    model = Song
    properties = [
        "id",
        "name",
    ]


class SongDetailEncoder(ModelEncoder):
    model = Song
    properties = [
        "id",
        "name",
        "artist",
    ]

    encoders = {
        "artist": ArtistDetailEncoder(),
    }
